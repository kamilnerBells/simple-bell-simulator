# Serial Port

In order to receive characters from the simulator sensior interface we need to connect to the serial port.

Use the "sensorPort" setting in config.yaml to configure which device to use for this.

## OS emulation of serial port

We can simulate the serial port device at the OS level on Linux by for testing purposes using socat to establish a virtual pty in a new window:

`$ socat -d -d -,raw,echo=0 PTY,link=/tmp/ttyUSB0,raw,echo=`

In the socat window, typing characters will send them to the app.
The sensorPort setting in config.yaml will need to be configured to /tmp/ttyUSB
