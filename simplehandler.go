package main

import (
	"log"

	"gitlab.com/kamilnerBells/bellsimlib"
)

// simpleHandler is an implementation of a handler
// It will receive bell events and instruct the ringer to ring the bell
type simpleHandler struct {
	bellsimlib.BellHandler
}

func newSimpleHandler() *simpleHandler {
	return &simpleHandler{}
}

func (mh *simpleHandler) Handle(bell *bellsimlib.Bell) {
	if bell != nil {
		log.Printf("Received Bell: %s\n", bell.Name)
		// Tell bell to ring
		bell.Ring()
	}
}
