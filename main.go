package main

import (
	"fmt"
	"log"
	"os"

	"github.com/jacobsa/go-serial/serial"
	"gitlab.com/kamilnerBells/bellsimlib"
)

func main() {

	// Get configuration
	config, err := bellsimlib.LoadConfig("config.yaml")

	if err != nil {
		fmt.Print(err)
		os.Exit(3)
	}

	// Construct bells from configuration
	var sensors []*bellsimlib.BellSensor
	var bells []bellsimlib.Bell

	// Instantiate synth ringer
	sr, _ := bellsimlib.NewSynthRinger(len(config.Bells), config.SynthSoundfont, 1, 12)

	// Instantiate bells based on configuration
	for i, bell := range config.Bells {
		sensor, _ := bellsimlib.NewBellSensor([]byte(bell.SensorChar)[0], bell.SensorDelay)
		sensors = append(sensors, sensor)
		newBell, _ := bellsimlib.NewBell(bell.Name, sensors[i], &sr)
		sr.SetNote(bell.Name, bell.Note) // Set SynthRinger note map based on configuration
		bells = append(bells, *newBell)
	}

	handler := newSimpleHandler()

	// Set up serial port.
	options := serial.OpenOptions{
		PortName:        config.SensorPort.Device,
		BaudRate:        config.SensorPort.BaudRate,
		DataBits:        config.SensorPort.DataBits,
		StopBits:        config.SensorPort.StopBits,
		MinimumReadSize: config.SensorPort.MinRead,
	}

	// Open the port.
	serialPort, err := serial.Open(options)
	if err != nil {
		log.Fatalf("serial.Open: %v", err)
	}

	// Make sure to close it later.
	defer serialPort.Close()

	simulator, _ := bellsimlib.NewSensorInterface(serialPort, bells[:], handler)

	simulator.Start()
}
