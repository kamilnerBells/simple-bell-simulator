# Simple Bell Simulator

This application aims to provide a simple bell simulator using the bellsimlib libraries, which interfaces with a Bagley MBI system, such as the Liverpool Simulator.

The application generates and plays simulated data that would be received on a serial interface with a real MBI system.

The primary aim of this project is to provide a simple capability that can be installed onto a small computing device, such as a Raspberry Pi,
which can be permanently installad alongside the bell simulator hardware and a suitable audio amplifier/speaker setup in the bell tower.

This application is written in Go, which can be installed on most Debian based linux distros as follows:

```bash
sudo apt install golang
```

You will need to install the serial and fluidsynth libraries. The fluidsynth library needs libfluidsynth1
You will also need the bellsimlib and scheduler libraries which are related development projects. Currently you will need repository permission to fetch these libraries.

On Ubuntu or other Debian based Linux distros you should be able to install this as follows:

```bash
sudo apt install libfluidsynth1
sudo apt install libfluidsynth-dev
```

You can then install the Go libraries:

```bash
go get github.com/jacobsa/go-serial/serial
go get github.com/sqweek/fluidsynth
go get gitlab.com/kamilnerBells/scheduler
go get gitlab.com/kamilnerBells/bellsimlib
```

The serial device and device configuration for the interface along with the characteristics (e.g. bell note, delay, sensor character) of each bell need to be configured in config.yaml.
For usb based serial convertors, the device is usually `/dev/ttyUSB0` on a typical linux distro such as Raspbian on a Raspberry Pi.
