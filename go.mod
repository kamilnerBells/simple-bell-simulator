module gitlab.com/kamilnerBells/simple-bell-simulator

go 1.14

require (
	github.com/VividCortex/ewma v1.1.1 // indirect
	github.com/certifi/gocertifi v0.0.0-20200211180108-c7c1fbc02894 // indirect
	github.com/cockroachdb/cockroach v19.2.5+incompatible // indirect
	github.com/cockroachdb/errors v1.2.4 // indirect
	github.com/cockroachdb/logtags v0.0.0-20190617123548-eb05cc24525f // indirect
	github.com/cockroachdb/ttycolor v0.0.0-20180709150743-a1d5aaeb377d // indirect
	github.com/codahale/hdrhistogram v0.0.0-20161010025455-3a0bb77429bd // indirect
	github.com/coreos/etcd v3.3.20+incompatible // indirect
	github.com/coreos/go-semver v0.3.0 // indirect
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf // indirect
	github.com/coreos/pkg v0.0.0-20180928190104-399ea9e2e55f // indirect
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/getsentry/raven-go v0.2.0 // indirect
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/grpc-ecosystem/grpc-gateway v1.14.3 // indirect
	github.com/jacobsa/go-serial v0.0.0-20180131005756-15cf729a72d4
	github.com/kataras/chronos v0.0.1 // indirect
	github.com/lightstep/lightstep-tracer-go v0.20.0 // indirect
	github.com/openzipkin-contrib/zipkin-go-opentracing v0.4.5 // indirect
	github.com/petermattis/goid v0.0.0-20180202154549-b0b1615b78e5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/prometheus/client_golang v1.5.1 // indirect
	github.com/rcrowley/go-metrics v0.0.0-20200313005456-10cdbea86bc0 // indirect
	github.com/rubrikinc/kronos v0.0.0-20200110173914-158235f5a9fb // indirect
	github.com/sasha-s/go-deadlock v0.2.0 // indirect
	github.com/scaledata/etcd v3.3.8+incompatible // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/sqweek/fluidsynth v0.0.0-20151217121601-462ff346de79 // indirect
	github.com/xiang90/probing v0.0.0-20190116061207-43a291ad63a2 // indirect
	gitlab.com/kamilnerBells/bellsimlib v1.0.0
	gitlab.com/kamilnerBells/scheduler v0.1.0 // indirect
	go.uber.org/zap v1.14.1 // indirect
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	google.golang.org/grpc v1.28.1 // indirect
)
